### Android-Apps 
Nachfolgend folgt eine Übersicht datenschutzfreundlicher Android-Apps (die laufen natürlich auch auf LineageOS / /e/ OS): 

### Browser 

[Fennec Browser](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/): Firefox Browser, bei dem der   Telemetriedatenversand standardmäßig deaktiviert ist, es werden allerdings weiterhin Verbindungen zu Mozilla und Google aufgebaut.   

[Tor Browser](https://www.torproject.org/download/#android): Der Tor-Browser für Android. 

[Privacy Browser](https://f-droid.org/en/packages/com.stoutner.privacybrowser.standard/): Gute datenschutzfreundliche Alternative. 

### Mail-Apps 

[FairEmail](https://f-droid.org/en/packages/eu.faircode.email/): Datenschutzfreundliche App mit intuitiver Benutzeroberfläche. 

[K-9 Mail](https://f-droid.org/packages/com.fsck.k9/): Datenschutzfreundliche Mail-App. 

### Messenger 

[Briar](https://f-droid.org/de/packages/org.briarproject.briar.android/): Datenschutzfreundlicher Peer-to-Peer-Messenger. 

[Conversations(XMPP)](https://f-droid.org/packages/eu.siacs.conversations/): Nativer XMPP-Client für Android, allerdings kostenpflichtig. 

[Session](https://getsession.org/): Für alle, die eine herkömmliche Nutzererfahrung erleben, aber dennoch auf ihre Privatssphäre achten möchten. 

[Silence](https://f-droid.org/packages/org.smssecure.smssecure/): (Verschlüsselte) SMS. 